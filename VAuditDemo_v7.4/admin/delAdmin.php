<?php
include_once('../sys/config.php');

if (isset($_SESSION['admin']) && !empty($_GET['id'])) {

    $clean_id = clean_input($_GET['id']);
    $query = "DELETE FROM admin WHERE admin_id = '$clean_id' LIMIT 1";
    $conn->query($query);
    $conn->close();
    header('Location: manageAdmin.php');
}
else {
    not_find($_SERVER['PHP_SELF']);
}   
?>
